class Point:

    def __new__(cls, *args):
        print("Вызов метода __new__ для", str(cls))
        return super().__new__(cls)
        pass

    def __init__(self, x, y):
        self.x = x
        self.y = y
        print("Вызов метода __init__ для", str(self))

pt = Point(1,2)
print(pt)
print(object.__dict__)