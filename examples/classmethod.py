class Point:
    MIN_COORDS = 0
    MAX_COORDS = 100

    @classmethod
    def validetion(cls, arg):
        return cls.MIN_COORDS <= arg <= cls.MAX_COORDS
    
    def __init__(self, x, y):
        self.x = self.y = 0
        if self.validetion(x) and self.validetion(y):
            self.x = x
            self.y = y
    
    def get_coords(self):
        return self.x, self.y
    
point1 = Point(5,200)
print(point1.get_coords())
