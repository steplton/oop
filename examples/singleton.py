class DataBase:
    __instance = None

    def __new__(cls, *args):
        if cls.__instance is None:
            cls.__instance = super().__new__(cls)
        return cls.__instance

    def __init__(self, user, pasw, port):
        self.user = user
        self.pasw = pasw
        self.port = port

    def __del__(self):
        DataBase.__instance = None

    def connect(self):
        print(f"Соединение с БД установлено, {self.user}, {self.pasw}, {self.port}")

    def close(self):
        print("Соединение с БД закрыто")

    def read(self):
        print(f"Чтение {data}")

us1 = 



