class Cars:

    def __init__(self, model, color):
        self.model = model
        self.color = color
        
    def get_attr(self):
        return self.model, self.color


car1 = Cars("opel", "red")
car2 = Cars("mazda", "black")

print(car1.get_attr())

